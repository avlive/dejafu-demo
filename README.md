# dejafu-demo

[![Hackage](https://img.shields.io/hackage/v/dejafu-demo.svg?logo=haskell)](https://hackage.haskell.org/package/dejafu-demo)
[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

See README for more info
