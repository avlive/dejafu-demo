module SkipChan (SkipChan, newSkipChan, getSkipChan, putSkipChan) where

{-|  Example taken from http://hackage.haskell.org/package/base-4.14.0.0/docs/Control-Concurrent-MVar.html

Consider the following concurrent data structure, a skip channel. This is a channel for an intermittent source of high bandwidth information (for example, mouse movement events.) Writing to the channel never blocks, and reading from the channel only returns the most recent value, or blocks if there are no new values. Multiple readers are supported with a dupSkipChan operation.

-}

import Prelude hiding (readMVar, newMVar, newEmptyMVar, MVar, putMVar, tryTakeMVar, takeMVar)
import Control.Concurrent.Classy


newtype SkipChan m a
    = SkipChan (MVar m a)


newSkipChan :: MonadConc m => m (SkipChan m a)
newSkipChan = do
    mvar <- newEmptyMVar
    return (SkipChan mvar)


getSkipChan :: MonadConc m => SkipChan m a -> m a
getSkipChan (SkipChan mvar) = do
    takeMVar mvar


putSkipChan :: MonadConc m => SkipChan m a -> a -> m ()
putSkipChan (SkipChan mvar) a = do
    _ <- tryTakeMVar mvar
    putMVar mvar a


data NotImplementedException = NotImplementedException
    deriving Show

instance Exception NotImplementedException

    -- dupSkipChan :: SkipChan a -> IO (SkipChan a)
