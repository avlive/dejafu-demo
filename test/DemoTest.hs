{-# LANGUAGE ScopedTypeVariables #-}
module DemoTest (spec_all) where

import Test.Tasty.Hspec
import SkipChan
import Test.DejaFu
import Control.Concurrent.Classy


spec_all :: Spec
spec_all =
  describe "SkipChan" $ do
    describe "newSkipChan" $ do
      it "does not block" $
        shouldAutocheck $ do
          (_ :: SkipChan m ()) <- newSkipChan
          return ()

    describe "getSkipChan" $ do
      it "blocks when reading from an empty channel" $
        should deadlocksAlways $ do
          (chan :: SkipChan m ()) <- newSkipChan
          getSkipChan chan
      it "reads the latest value" $ do
        shouldProduce "A" $ do
          chan <- newSkipChan
          putSkipChan chan "A"
          getSkipChan chan
      it "returns the most recent value" $ do
        shouldProduce "B" $ do
          chan <- newSkipChan
          putSkipChan chan "A"
          putSkipChan chan "B"
          getSkipChan chan
      it "blocks when there is no new value" $ do
        should deadlocksAlways $ do
          chan <- newSkipChan
          putSkipChan chan "A"
          _ <- getSkipChan chan
          getSkipChan chan

    describe "putSkipChan" $ do
      it "does not block when channel is empty" $ do
        shouldAutocheck $ do
          chan <- newSkipChan
          putSkipChan chan ()
      it "does not block when channel already has a value" $ do
        shouldAutocheck $ do
          chan <- newSkipChan
          putSkipChan chan "A"
          putSkipChan chan "B"

    describe "integration test" $
      let
        writeStep :: MonadConc m => SkipChan m Int -> Int -> m ()
        writeStep chan nextInt = do
          putSkipChan chan nextInt
          if nextInt >= 10
            then return ()
            else writeStep chan (nextInt + 1)
      in do
      it "readers never see value out of order" $ do
        should exceptionsNever $
          let
            failIfNextIsNotGreater :: MonadConc m => SkipChan m Int -> Maybe Int -> m ()
            failIfNextIsNotGreater chan lastInt = do
              newInt <- getSkipChan chan
              case lastInt of
                Just n | newInt <= n ->
                  throw (TestFailure $ "got " ++ show newInt ++ " after " ++ show n)
                _ -> failIfNextIsNotGreater chan (Just newInt)
          in do
          chan <- newSkipChan
          _ <- fork (writeStep chan 0)
          failIfNextIsNotGreater chan Nothing

      it "can skip values" $ do
        should (somewhereTrue (== Right True)) $
          let
            untilValueIsSkipped :: MonadConc m => SkipChan m Int -> Maybe Int -> m Bool
            untilValueIsSkipped chan lastInt = do
              newInt <- getSkipChan chan
              case lastInt of
                Just n | newInt > (n+1) -> return True
                _ | newInt >= 10 -> return False
                _ -> untilValueIsSkipped chan (Just newInt)
          in do
          chan <- newSkipChan
          _ <- fork (writeStep chan 0)
          untilValueIsSkipped chan Nothing


should :: Show b => ProPredicate a b -> Program pty IO a -> Expectation
should predicate action =
  dejafu "" predicate action `shouldReturn` True


shouldAutocheck :: (Eq a, Show a) => Program pty IO a -> Expectation
shouldAutocheck action =
  autocheck action `shouldReturn` True


shouldProduce :: (Eq a, Show a) => a -> Program pty IO a -> Expectation
shouldProduce expected action =
  dejafus
    [ ( "returns " ++ show expected, gives' [expected] ) ]
    action
    `shouldReturn` True

newtype TestFailure
  = TestFailure String
  deriving Show

instance Exception TestFailure
