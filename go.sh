#!/bin/sh

which ghcid || cabal install ghcid

ghcid \
    --command "cabal repl dejafu-demo-test" \
    --test "main" \
    --warnings
