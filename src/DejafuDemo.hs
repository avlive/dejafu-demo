{- |
Copyright: (c) 2020 Aaron VonderHaar
SPDX-License-Identifier: MIT
Maintainer: Aaron VonderHaar <gruen0aermel@gmail.com>

See README for more info
-}

module DejafuDemo
       ( someFunc
       ) where


someFunc :: IO ()
someFunc = putStrLn ("someFunc" :: String)
